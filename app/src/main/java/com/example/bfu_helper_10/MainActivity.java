package com.example.bfu_helper_10;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    List<DayOfSchedule> days = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        RelativeLayout main_layout = (RelativeLayout) findViewById(R.id.main_layout);

        setInitialData();
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.list);
        DataAdapter adapter = new DataAdapter(this, days);
        //

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.page_1:
                        main_layout.setBackgroundColor(Color.parseColor("#E0BDFB"));
                        recyclerView.setAdapter(null);
                        break;
                    case R.id.page_2:
                        main_layout.setBackgroundColor(Color.parseColor("#BDC0FB"));
                        recyclerView.setAdapter(null);
                        break;
                    case R.id.page_3:
                        main_layout.setBackgroundColor(Color.parseColor("#FFFFFF"));
                        recyclerView.setAdapter(adapter);

                        break;
                    default:
                        break;
                }
                return true;
            }
        });
    }

    private void setInitialData() {
        List<Subject> subjectsMonday = new ArrayList<>();
        subjectsMonday.add(new Subject("8:30 - 10:00   ","Теория вероятностей и математическая статистика (лек.)", R.drawable.probability));
        subjectsMonday.add(new Subject("10:10 - 11:40   ","Теория вероятностей и математическая статистика (пр.)", R.drawable.probability));
        subjectsMonday.add(new Subject("13:40 - 15:10   ","Уравнения математической физики 1гр.", R.drawable.phys));
        subjectsMonday.add(new Subject("15:20 - 16:50   ","Уравнения математической физики 1гр.", R.drawable.phys));
        days.add(new DayOfSchedule(" Понедельник", "Ноябрь ", 16, subjectsMonday));

        List<Subject> subjectsTuesday = new ArrayList<>();
        subjectsTuesday.add(new Subject("8:30 - 11:40   ","АИПОВК", R.drawable.program));
        subjectsTuesday.add(new Subject("15:20 - 17:00   ","Функциональный анализ функций комплексного переменного(лек.) 219 ауд.", R.drawable.science));
        days.add(new DayOfSchedule("Вторник", "Ноябрь ", 17, subjectsTuesday));

        List<Subject> subjectsWednesday = new ArrayList<>();
        subjectsWednesday.add(new Subject("8:30 - 10:00   ","Коммуникационный модуль", R.drawable.comm_2));
        subjectsWednesday.add(new Subject("10:10 - 11:40   ","Коммуникационный модуль", R.drawable.comm_2));
        days.add(new DayOfSchedule(" Среда", "Ноябрь ", 18, subjectsWednesday));

        List<Subject> tues = new ArrayList<>();
        tues.add(new Subject("8:30 - 11:40   ","Математический анализ", R.drawable.science));
        tues.add(new Subject("15:20 - 17:00   ","Математический анализ", R.drawable.science));
        tues.add(new Subject("15:20 - 17:00   ","Математический анализ", R.drawable.science));
        tues.add(new Subject("15:20 - 17:00   ","Математический анализ", R.drawable.science));
        days.add(new DayOfSchedule(" Четверг", "Ноябрь ", 19, tues));
    }
}