package com.example.bfu_helper_10;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {

    private final LayoutInflater inflater;
    private final List<DayOfSchedule> daysOfSchedule;
    private final RecyclerView.RecycledViewPool viewPool = new RecyclerView.RecycledViewPool();

    DataAdapter(Context context, List<DayOfSchedule> daysOfSchedule) {
        this.inflater = LayoutInflater.from(context);
        this.daysOfSchedule = daysOfSchedule;
    }

    @NonNull
    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataAdapter.ViewHolder holder, int position) {
        DayOfSchedule dos = daysOfSchedule.get(position);
        holder.date.setText(dos.getDayOfWeek() + ", " + dos.getDate() + " " + dos.getMonth());
        LinearLayoutManager layoutManager = new LinearLayoutManager(holder.rv.getContext(), LinearLayoutManager.VERTICAL, false);
        layoutManager.setInitialPrefetchItemCount(dos.getSubjects().size());
        CardAdapter cardAdapter = new CardAdapter(dos.getSubjects());
        holder.rv.setLayoutManager(layoutManager);
        holder.rv.setAdapter(cardAdapter);
        holder.rv.setRecycledViewPool(viewPool);
    }

    @Override
    public int getItemCount() {
        return daysOfSchedule == null ? 0 : daysOfSchedule.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        final TextView date;
        final RecyclerView rv;
        ViewHolder(View view) {
            super(view);
            date = (TextView)view.findViewById(R.id.date);
            rv = (RecyclerView) view.findViewById(R.id.cardId);
        }
    }
}
